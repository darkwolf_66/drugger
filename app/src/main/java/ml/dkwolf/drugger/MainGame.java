package ml.dkwolf.drugger;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ml.dkwolf.drugger.game.City;
import ml.dkwolf.drugger.game.Game;
import ml.dkwolf.drugger.game.LayoutStructures.CityListItem;
import ml.dkwolf.drugger.game.LayoutStructures.CityListItemAdapter;

public class MainGame extends AppCompatActivity {
    TextView money_show;
    ListView citysListView;
    ArrayList<CityListItem> cityListItems;
    private static CityListItemAdapter cityListItemAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);
        //SetTabMenu
        TabLayout   tabLayout = (TabLayout) findViewById(R.id.main_tab_travel_content); // get the reference of TabLayout
        TabLayout.Tab firstTab = tabLayout.newTab(); // Create a new Tab names

        // Load Logo
        ImageView logoView = (ImageView) findViewById(R.id.logoView);
        Glide.with(this).load(R.drawable.logo).into(logoView);
        ImageView mapIconView = (ImageView) findViewById(R.id.mapIconView);
        Glide.with(this).load(R.drawable.location).into(mapIconView);
        //Mostra o dinheiro ingame
        money_show = (TextView) findViewById(R.id.money_view);
        //Lista as cidades
        citysListView = (ListView) findViewById(R.id.citys_list);
        cityListItems =  citysToListItem();

        cityListItemAdapter = new CityListItemAdapter(cityListItems, getApplicationContext());
        citysListView.setAdapter(cityListItemAdapter);

        updateViews ();
    }

    public ArrayList<CityListItem> citysToListItem(){
        ArrayList<CityListItem> citysListItens = new ArrayList<CityListItem>();
        for (int i = 0; i < Game.getCitys().size(); i++){
            City tempCity = Game.getCitys().get(i);
            citysListItens.add(new CityListItem(tempCity.getName(), tempCity.getId()));
        }
        return citysListItens;
    }

    public void updateViews () {
        money_show.setText(Game.getMoney().toString());
    }

}
