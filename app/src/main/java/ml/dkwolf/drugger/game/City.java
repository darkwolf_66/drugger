package ml.dkwolf.drugger.game;

import java.util.ArrayList;

/**
 * Created by willm on 18/10/2017.
 */

public class City {
    private ArrayList<Product> products;
    final private String name;
    final private int id;

    public City(ArrayList<Product> products, String name, int id) {
        this.products = products;
        aleatoryzeProducts();
        this.name = name;
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public void aleatoryzeProducts (){
        for (int i = 0; i < products.size(); i++) {
            products.get(i).aleatoryze();
        }
    }
}
