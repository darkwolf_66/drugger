package ml.dkwolf.drugger.game;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Created by willm on 16/10/2017.
 */

public class Product {
    private int id;
    private String name;
    private BigDecimal cost;
    private int maxCost;
    private int minCost;
    private int inventory;

    public Product(int id, String name, BigDecimal cost, int maxCost, int minCost, int inventory) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.maxCost = maxCost;
        this.minCost = minCost;
        this.inventory = inventory;
    }

    public BigDecimal newCost (){
        cost = new BigDecimal(new Random().nextInt((maxCost-minCost)+1)+minCost);
        return cost;
    }
    public int newInventory (){
        inventory = new Random().nextInt((1000-100)+1)+100;
        return inventory;
    }

    public void aleatoryze(){
        newCost();
        newInventory();
    }
    public BigDecimal getCost() {
        return cost;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }
    public void addInventory(int units) {
        this.inventory += units;
    }
    public void subtractFromInventory(int units) {
        this.inventory -= units;
    }
}
