package ml.dkwolf.drugger.game.LayoutStructures;

import android.view.View;
import android.widget.Button;

import ml.dkwolf.drugger.game.Game;

/**
 * Created by willm on 25/10/2017.
 */

public class CityListItem {
    String name;
    final int cityId;
    Button.OnClickListener getGoToButtonListener;

    public CityListItem(String name, final int cityId) {
        this.name = name;
        this.cityId = cityId;
        this.getGoToButtonListener = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.goToCity(cityId);
            }
        };
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCityId() {
        return cityId;
    }

}
