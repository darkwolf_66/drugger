package ml.dkwolf.drugger.game.LayoutStructures;


import android.view.View;
import android.widget.Button;

import ml.dkwolf.drugger.game.Game;

/**
 * Created by willm on 25/10/2017.
 */

public class DrugListItem {
    String drug;
    String price;
    final int product_id;
    Button.OnClickListener buy;
    Button.OnClickListener sell;

    public DrugListItem(String drug, String price, final int product_id) {
        this.drug = drug;
        this.price = price;
        this.product_id = product_id;
        buy = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.buyById(product_id);
            }
        };
        sell = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Game.buyById(product_id);
            }
        };
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getProduct_id() {
        return product_id;
    }
}
