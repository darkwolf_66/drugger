package ml.dkwolf.drugger.game;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by willm on 16/10/2017.
 */

public class GameSave {
    private BigDecimal money;
    private ArrayList citys;
    private ArrayList products;
    private int atualCity;

    public GameSave(BigDecimal money, ArrayList citys) {
        this.money = money;
        this.citys = citys;
    }

    public GameSave(BigDecimal money, ArrayList citys, ArrayList products, int atualCity) {
        this.money = money;
        this.citys = citys;
        this.atualCity = atualCity;
    }

    public int getAtualCity() {
        return atualCity;
    }

    public void setAtualCity(int atualCity) {
        this.atualCity = atualCity;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public ArrayList<City> getCitys() {
        return citys;
    }

    public void setCitys(ArrayList<City> citys) {
        this.citys = citys;
    }

    public ArrayList getProducts() {
        return products;
    }

    public void setProducts(ArrayList products) {
        this.products = products;
    }
}
