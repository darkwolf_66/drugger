package ml.dkwolf.drugger.game;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by willm on 16/10/2017.
 */

public class Game {
    private static GameSave save;

    public static boolean buy (Product val, int units){
        BigDecimal price = val.getCost().multiply(new BigDecimal(units));
        if(save.getMoney().compareTo(price) == 1){
            save.setMoney(save.getMoney().subtract(price));
            val.addInventory(units);
            return true;
        }
        return false;
    }
    public static boolean sell (Product val, int units){
        BigDecimal price = val.getCost().multiply(new BigDecimal(units));
        if(val.getInventory() >= units){
            save.setMoney(save.getMoney().add(price));
            val.subtractFromInventory(units);
            return true;
        }
        return false;
    }

    public static void newGame(GameSave gameSave) {
        save = gameSave;
    }

    public static BigDecimal getMoney() {
        return save.getMoney();
    }

    public static void setMoney(BigDecimal money) {
        save.setMoney(money);
    }

    public static ArrayList<City> getCitys() {
        return save.getCitys();
    }
    public static void setCitys(ArrayList citys) {
        save.setCitys(citys);
    }
    public static void buyById(int id){

    }
    public static void sellById(int id){

    }
    public static void goToCity(int cityId) {
        save.setAtualCity(cityId);
        save.getCitys().get(cityId-1).aleatoryzeProducts();
    }
    public static void setProducts(ArrayList products) {
        save.setProducts(products);
    }


}
