package ml.dkwolf.drugger.game.LayoutStructures;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import ml.dkwolf.drugger.R;

/**
 * Created by willm on 25/10/2017.
 */

public class DrugListItemAdapter extends ArrayAdapter<DrugListItem> {
    private ArrayList<DrugListItem> drugListItem;
    Context mContext;
    private static class ViewHolder {
        TextView drugName;
        TextView drugPrice;
        Button buy;
        Button sell;
    }
    public DrugListItemAdapter(ArrayList<DrugListItem> drugListItem, Context mContext) {
        super(mContext, R.layout.drug_list_item, drugListItem);
        this.drugListItem = drugListItem;
        this.mContext = mContext;
    }
    private int lastPosition = -1;
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DrugListItem drugListItem = getItem(position);
        ViewHolder viewHolder;

        final View result;
        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.drug_list_item, parent, false);
            viewHolder.drugName = (TextView) convertView.findViewById(R.id.drugName);
            viewHolder.drugPrice = (TextView) convertView.findViewById(R.id.drugPrice);
            viewHolder.buy = (Button) convertView.findViewById(R.id.buy);
            viewHolder.sell = (Button) convertView.findViewById(R.id.sell);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        @SuppressLint("ResourceType") Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.animator.up_from_button : R.animator.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.drugName.setText(drugListItem.getDrug());
        viewHolder.drugPrice.setText(drugListItem.getPrice());
        viewHolder.buy.setOnClickListener(drugListItem.buy);
        viewHolder.sell.setOnClickListener(drugListItem.sell);
        // Return the completed view to render on screen
        return convertView;
    }
}
