package ml.dkwolf.drugger.game.LayoutStructures;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import ml.dkwolf.drugger.R;

/**
 * Created by willm on 25/10/2017.
 */

public class CityListItemAdapter extends ArrayAdapter<CityListItem> {
    private ArrayList<CityListItem> cityListItens;
    Context mContext;
    private class ViewHolder {
        TextView name;
        int id;
        Button goToCity;
    }
    public CityListItemAdapter(ArrayList<CityListItem> cityListItens, Context mContext) {
        super(mContext, R.layout.city_list_item, cityListItens);
        this.cityListItens = cityListItens;
        this.mContext = mContext;
    }
    private int lastPosition = -1;
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CityListItem cityListItem = cityListItens.get(position);
        ViewHolder viewHolder;

        final View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.city_list_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.cityNameLabel);
            viewHolder.goToCity = (Button) convertView.findViewById(R.id.goToCityButton);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }
        //@SuppressLint("ResourceType") Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.animator.up_from_button : R.animator.down_from_top);
        //result.startAnimation(animation);

        viewHolder.name.setText(cityListItem.getName());

        viewHolder.goToCity.setOnClickListener(cityListItem.getGoToButtonListener);
        // Return the completed view to render on screen
        return convertView;
    }
}
