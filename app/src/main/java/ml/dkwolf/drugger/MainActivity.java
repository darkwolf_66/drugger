package ml.dkwolf.drugger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;

import ml.dkwolf.drugger.game.City;
import ml.dkwolf.drugger.game.Game;
import ml.dkwolf.drugger.game.GameSave;
import ml.dkwolf.drugger.game.Product;

public class MainActivity extends AppCompatActivity {
    Button newGame;
    Locale myLocale;
    Spinner languageSelect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView logoView = (ImageView) findViewById(R.id.logoView);
        Glide.with(this).load(R.drawable.logo).into(logoView);
        ImageView bottomArt = (ImageView) findViewById(R.id.bottonArt);
        Glide.with(this).load(R.drawable.bottom_art).into(bottomArt);


        newGame = (Button) findViewById(R.id.new_game);
        newGame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createNewGame();
                startGame();
                finish();
            }
        });
    }

    public void startGame(){
        Intent gameActivity = new Intent(this, MainGame.class);
        startActivity(gameActivity);
    }
    public void createNewGame(){
        ArrayList cities = new ArrayList<City>();
        ArrayList products = new ArrayList<Product>();
        products.add(new Product(1, getResources().getString(R.string.drug_vicodin), new BigDecimal("3"),30, 3, 100));
        products.add(new Product(2, getResources().getString(R.string.drug_adderall), new BigDecimal("4"), 40, 4, 100));
        products.add(new Product(3, getResources().getString(R.string.drug_ambien), new BigDecimal("4"), 40, 4, 100));
        products.add(new Product(4, getResources().getString(R.string.drug_prozac), new BigDecimal("5"), 50, 5, 100));
        products.add(new Product(5, getResources().getString(R.string.drug_ritalin), new BigDecimal("5"), 50, 5, 100));
        products.add(new Product(6, getResources().getString(R.string.drug_xanax), new BigDecimal("5"), 50, 5, 100));
        products.add(new Product(7, getResources().getString(R.string.drug_ecstasy), new BigDecimal("6"), 60, 6, 100));
        products.add(new Product(8, getResources().getString(R.string.drug_methadone), new BigDecimal("6"), 60, 6, 100));
        products.add(new Product(9, getResources().getString(R.string.drug_nitrous_oxide), new BigDecimal("6"), 60, 6, 100));
        products.add(new Product(10, getResources().getString(R.string.drug_percocet), new BigDecimal("7"), 70, 7, 100));
        products.add(new Product(11, getResources().getString(R.string.drug_bath_salts), new BigDecimal("8"), 80, 8, 100));
        products.add(new Product(12, getResources().getString(R.string.drug_codeine), new BigDecimal("8"), 80, 8, 100));
        products.add(new Product(13, getResources().getString(R.string.drug_morphine), new BigDecimal("8"), 80, 8, 100));
        products.add(new Product(14, getResources().getString(R.string.drug_ghb), new BigDecimal("10"), 100, 10, 100));
        products.add(new Product(15, getResources().getString(R.string.drug_lsd), new BigDecimal("10"), 100, 10, 100));
        products.add(new Product(16, getResources().getString(R.string.drug_valium), new BigDecimal("10"), 100, 10, 100));
        products.add(new Product(17, getResources().getString(R.string.drug_dmt), new BigDecimal("15"), 150, 15, 100));
        products.add(new Product(18, getResources().getString(R.string.drug_ketamine), new BigDecimal("15"), 150, 15, 100));
        products.add(new Product(19, getResources().getString(R.string.drug_pcp), new BigDecimal("20"), 200, 20, 100));
        products.add(new Product(20, getResources().getString(R.string.drug_roxicodone), new BigDecimal("20"), 200, 20, 100));
        products.add(new Product(21, getResources().getString(R.string.drug_fentanyl), new BigDecimal("25"), 250, 25, 100));
        products.add(new Product(22, getResources().getString(R.string.drug_psilocybin), new BigDecimal("35"), 350, 35, 100));
        products.add(new Product(23, getResources().getString(R.string.drug_oxycontin), new BigDecimal("40"), 400, 40, 100));
        products.add(new Product(24, getResources().getString(R.string.drug_crack), new BigDecimal("45"), 450, 45, 100));
        products.add(new Product(25, getResources().getString(R.string.drug_marijuana), new BigDecimal("45"), 450, 45, 100));
        products.add(new Product(26, getResources().getString(R.string.drug_cocaine), new BigDecimal("50"), 500, 50, 100));
        products.add(new Product(27, getResources().getString(R.string.drug_heroin), new BigDecimal("50"), 500, 50, 100));
        products.add(new Product(28, getResources().getString(R.string.drug_meth), new BigDecimal("70"), 700, 70, 100));

        cities.add(new City(products,"São Paulo", 1));
        cities.add(new City(products,"Rio de Janeiro", 2));
        cities.add(new City(products,"Porto Alegre",3) );
        cities.add(new City(products,"Curitiba",4));
        cities.add(new City(products,"Brasilia",5));
        cities.add(new City(products,"Porto Alegre",6) );
        cities.add(new City(products,"Curitiba",7));
        cities.add(new City(products,"Brasilia",8));
        cities.add(new City(products,"Porto Alegre",9) );
        cities.add(new City(products,"Curitiba",10));
        cities.add(new City(products,"Brasilia",11));
        cities.add(new City(products,"Curitiba",12));
        cities.add(new City(products,"Brasilia",13));
        cities.add(new City(products,"Porto Alegre",14) );
        cities.add(new City(products,"Curitiba",15));
        cities.add(new City(products,"Brasilia",16));
        cities.add(new City(products,"Curitiba",17));
        cities.add(new City(products,"Brasilia",18));
        cities.add(new City(products,"Porto Alegre",19) );
        cities.add(new City(products,"Curitiba",20));
        cities.add(new City(products,"Brasilia",21));

        Game.newGame(new GameSave(new BigDecimal(300), cities));
    }
}